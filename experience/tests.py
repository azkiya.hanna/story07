from django.test import TestCase,Client, LiveServerTestCase
from django.urls import reverse,resolve
from .views import ExperienceFunction
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class ExperienceTest(TestCase):
    def test_apakah_ada_slash_experience(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code,200)
    def test_apakah_file_html_experience_digunakan(self):
        c = Client()
        response = c.get('')
        self.assertTemplateUsed(response, 'experience.html')
    def test_apakah_fungsi_status_dijalankan(self):
        response = resolve(reverse('experience'))
        self.assertEqual(response.func,ExperienceFunction)
    def test_apakah_ada_toggle_untuk_ganti_tema(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('switch',content)
    def test_apakah_ada_accordian(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('accordian',content)
        self.assertIn('card-header',content)
        self.assertIn('card-body',content)
    def test_apakah_ada_greetings(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('greetings',content)


class ExperienceFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_op = Options()
        chrome_op.add_argument('--no-sandbox'),
        chrome_op.add_argument('--headless'),
        chrome_op.add_argument('disable-dev-shm-usage'),
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_op)
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_apakah_title_sesuai(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        self.assertIn("About Azkiya",selenium.title)

    def test_apakah_toggle_bisa_diklik_dan_berganti_tema(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        # cek terlebih dahulu apakah benar awalnya bertema day,
        # jika bertema day, maka akan menggunakan file css bernama style.css
        self.assertIn('day',selenium.page_source)
        self.assertIn('style.css',selenium.page_source)

        
        # setelah di klik, tema day akan berubah menjadi night,
        # dan akan menggunakan file css style-night.css
        toggle_button = selenium.find_element_by_class_name("slider")
        toggle_button.click()
        self.assertIn('night',selenium.page_source)
        self.assertIn('style-night.css',selenium.page_source)


    def test_apakah_accordion_aktifitas_bisa_diklik(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        #sebelum di klik, maka semua accordion tertutup
        self.assertIn('display: none',selenium.page_source)

        #klik accordian activity sehingga accordion tersebut terbuka
        accordion_activity= selenium.find_element_by_name("activity")
        accordion_activity.click()
        self.assertNotIn('name= "activity" style= "display: none;"',selenium.page_source)

    def test_apakah_accordian_organisasi_bisa_diklik(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        #sebelum di klik, maka semua accordian tertutup
        self.assertIn('display: none',selenium.page_source)

        #klik accordion organization sehingga accordian tersebut terbuka
        accordion_organization= selenium.find_element_by_name("organization")
        accordion_organization.click()
        self.assertNotIn('name= "organization" style= "display: none;"',selenium.page_source)

    def test_apakah_accordian_achievements_bisa_diklik(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        selenium.get(self.live_server_url)

        #sebelum di klik, maka semua accordion tertutup
        self.assertIn('display: none',selenium.page_source)

        #klik accordion achievements sehingga accordian tersebut terbuka
        accordion_achievements= selenium.find_element_by_name("achievements")
        accordion_achievements.click()
        self.assertNotIn('name= "achievements" style=" display: none;"',selenium.page_source)

        
    




